import React from 'react';
import PropTypes from 'prop-types';
import {PayPalButton} from 'react-paypal-button'

class Modal extends React.Component {
  render() {
    // Render nothing if the "show" prop is false
    if(!this.props.show) {
      return null;
    }

    // The gray background
    const backdropStyle = {
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'rgba(0,0,0,0.3)',
      padding: 50,
    };

    // The modal "window"
    const modalStyle = {
      backgroundColor: '#fff',
      borderRadius: 5,
      maxWidth: 500,
      minHeight: 300,
      margin: '0 auto',
      padding: 30
    };

    return (
    <div>
        <div className="product popup">
        <div className="product-image">
      
        <div className="footer">
          <button onClick={this.props.onClose}>
          Close
          </button>      
        </div>

        </div>
        <p className="no-product"> No. of items : {this.props.totalItems}
       
        </p>
        <p className="product-price">{this.props.price}</p>
      
        <div className="product-action">
        <PayPalButton
               env='sandbox'
               sandboxID='AV7iXttMTAMn2dSjQd24gPD-atTgj78Tbv33y2mEHOrp189e8KTWlt5CFigpNNEeiaQu41O72T1XfU1W'
                amount={10000}
                currency='USD'
                commit={true}
            />
        </div>
      </div>
      <div className="overlay">&nbsp;</div>   
    </div>
    );
  }
}

Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  children: PropTypes.node
};

export default Modal;