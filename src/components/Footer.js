import React, { Component } from "react";

const Footer = props => {
  return (
    <footer>
      <p>
        &copy; 2019 <strong>krescentglobal</strong> -Mobile Store
      </p>
    </footer>
  );
};

export default Footer;
