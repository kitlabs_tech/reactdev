  export const data = {    
      "data": [        
        {
          "category": "Iphone",
          "id": 1,
          "image": 'https://imazing.com/img/visual/backups/top-iphone.png',
          "name": "Iphone - 7",
          "price": 40000
          },
          {
            "category": "Iphone",
            "id": 2,
            "image": 'https://media.wired.com/photos/5b22c5c4b878a15e9ce80d92/master/pass/iphonex-TA.jpg',
            "name": "Iphone - 10",
            "price": 90000
          },
          {
            "category": "Iphone",
            "id": 3,
            "image": 'https://media.wired.com/photos/5b22c5c4b878a15e9ce80d92/master/pass/iphonex-TA.jpg',
            "name": "Iphone - 10",
            "price": 80000
          },
          {
            "category": "Iphone",
            "id": 4,
            "image": 'https://media.wired.com/photos/5b22c5c4b878a15e9ce80d92/master/pass/iphonex-TA.jpg',
            "name": "Iphone - 10",
            "price": 95000
          },
          {
            "category": "Iphone",
            "id": 5,
            "image": 'https://media.wired.com/photos/5b22c5c4b878a15e9ce80d92/master/pass/iphonex-TA.jpg',
            "name": "Iphone - 10",
            "price": 96000
          }
      ]
        
    }

    export default data;