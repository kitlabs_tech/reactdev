# React Shopping Cart

Simple React Shopping Cart without using Redux

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)](http://krescentglobal.in:8015/) [![Github file size](https://img.shields.io/github/size/webcaetano/craft/build/phaser-craft.min.js.svg)](http://krescentglobal.in:8015/) [![David](https://img.shields.io/david/expressjs/express.svg)](http://krescentglobal.in:8015/)

![React Shopping Cart Preview](http://krescentglobal.in/reactproject/react-shopping/pic.png)

## Live Demo

[http://krescentglobal.in:8015/](http://krescentglobal.in:8015/)

## Setup

Goto the project folder and install required dependencies:

```
npm install
```

And run Webpack to watch for code changes and bundle js and scss files:

```
npm start
```

Project will be automatically open at http://krescentglobal.in:8015

For production build:

```
npm run build
```

### Buy me a Coffee
